﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Api.Data
{
    internal class DatabaseInitializer : CreateDatabaseIfNotExists<Context>
    {
        protected override void Seed(Context context)
        {
            var todos = new List<Todo>()
            {
                new Todo() { Task = "Plan todo list project" },
                new Todo() { Task = "Set up database" },
                new Todo() { Task = "Code API controllers" }
            };

            context.Todos.AddRange(todos);
            context.SaveChanges();
        }
    }
}