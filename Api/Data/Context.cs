﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Api.Data
{
    public class Context : DbContext
    {
        public DbSet<Todo> Todos { get; set; }
    }
}