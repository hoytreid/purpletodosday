﻿using Api.Data;
using Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Api.Controllers
{
    [EnableCors(origins: "http://localhost:8840", headers: "*", methods: "*")]
    public class TodosController : ApiController
    {
        private Context context = new Context();

        public IEnumerable<Todo> Get()
        {
            return context.Todos.ToList();
        }

        public Todo Get(int id)
        {
            return context.Todos
                .Where(t => t.Id == id)
                .SingleOrDefault();
        }

        public void Post(Todo todo)
        {
            context.Todos.Add(todo);
            context.SaveChanges();
        }

        public void Put(int id, Todo todo)
        {
            Todo updateTodo = context.Todos.Find(id);
            context.Entry(updateTodo).CurrentValues.SetValues(todo);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            Todo todo = new Todo() { Id = id };
            context.Entry(todo).State = EntityState.Deleted;
            context.SaveChanges();
        }
    }
}
