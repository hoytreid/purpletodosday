﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class Todo
    {
        [Key]
        public int Id { get; set; }
        public string Task { get; set; }
        public bool Complete { get; set; }
    }
}