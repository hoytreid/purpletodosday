var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
var TodoListComponent = /** @class */ (function () {
    function TodoListComponent() {
        this.delete = new EventEmitter();
        this.save = new EventEmitter();
    }
    TodoListComponent.prototype.onSaveTodo = function (todo) {
        this.save.emit(todo);
    };
    TodoListComponent.prototype.onDeleteTodo = function (todo) {
        this.delete.emit(todo);
    };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], TodoListComponent.prototype, "todos", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], TodoListComponent.prototype, "showCompleted", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], TodoListComponent.prototype, "delete", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], TodoListComponent.prototype, "save", void 0);
    TodoListComponent = __decorate([
        Component({
            selector: 'app-todo-list',
            templateUrl: './todo-list.component.html',
            styleUrls: ['./todo-list.component.css']
        })
    ], TodoListComponent);
    return TodoListComponent;
}());
export { TodoListComponent };
//# sourceMappingURL=todo-list.component.js.map