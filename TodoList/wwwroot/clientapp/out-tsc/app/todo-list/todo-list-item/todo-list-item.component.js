var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../../todo';
var TodoListItemComponent = /** @class */ (function () {
    function TodoListItemComponent() {
        this.delete = new EventEmitter();
        this.save = new EventEmitter();
        this.editing = false;
    }
    TodoListItemComponent.prototype.toggleComplete = function () {
        this.save.emit(this.todo);
    };
    TodoListItemComponent.prototype.editTodo = function () {
        this.editing = true;
        this.backupTodo = JSON.parse(JSON.stringify(this.todo));
    };
    TodoListItemComponent.prototype.cancelEdit = function () {
        this.editing = false;
        this.todo = this.backupTodo;
    };
    TodoListItemComponent.prototype.saveTodo = function () {
        this.editing = false;
        this.save.emit(this.todo);
    };
    TodoListItemComponent.prototype.deleteTodo = function () {
        this.delete.emit(this.todo);
    };
    __decorate([
        Input(),
        __metadata("design:type", Todo)
    ], TodoListItemComponent.prototype, "todo", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], TodoListItemComponent.prototype, "delete", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], TodoListItemComponent.prototype, "save", void 0);
    TodoListItemComponent = __decorate([
        Component({
            selector: 'app-todo-list-item',
            templateUrl: './todo-list-item.component.html',
            styleUrls: ['./todo-list-item.component.css']
        })
    ], TodoListItemComponent);
    return TodoListItemComponent;
}());
export { TodoListItemComponent };
//# sourceMappingURL=todo-list-item.component.js.map