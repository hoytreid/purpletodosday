var Todo = /** @class */ (function () {
    function Todo(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return Todo;
}());
export { Todo };
//# sourceMappingURL=todo.js.map