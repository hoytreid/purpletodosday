import { TestBed, inject } from '@angular/core/testing';
import { TodoService } from './todo.service';
describe('TodoService', function () {
    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [TodoService]
        });
    });
    it('should be created', inject([TodoService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=todo.service.spec.js.map