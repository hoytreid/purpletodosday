var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { TodoService } from '../todo.service';
var TodosComponent = /** @class */ (function () {
    function TodosComponent(todoService) {
        this.todoService = todoService;
    }
    TodosComponent.prototype.ngOnInit = function () {
        this.getTodos();
    };
    TodosComponent.prototype.getTodos = function () {
        var _this = this;
        this.todoService.getTodos()
            .subscribe(function (todos) { return _this.todos = todos; });
    };
    //onsubmit(task: string): void {
    //    task = task.trim();
    //    this.todoservice.addtodo({ task } as todo)
    //        .subscribe(() => this.gettodos());
    //}
    TodosComponent.prototype.onDelete = function (todo) {
        this.todos = this.todos.filter(function (t) { return t !== todo; });
        this.todoService.deleteTodo(todo).subscribe();
    };
    TodosComponent.prototype.onEdit = function (todo) {
        this.editingTodo = todo;
        this.backUp = JSON.parse(JSON.stringify(todo));
    };
    TodosComponent.prototype.onCheck = function (todo) {
        this.todoService.editTodo(todo).subscribe();
    };
    TodosComponent.prototype.onSave = function () {
        this.todoService.editTodo(this.editingTodo).subscribe();
        this.editingTodo = null;
    };
    TodosComponent.prototype.onCancel = function () {
        this.editingTodo.task = this.backUp.task;
        this.editingTodo = null;
    };
    TodosComponent = __decorate([
        Component({
            selector: 'app-todos',
            templateUrl: './todos.component.html',
            styleUrls: ['./todos.component.css']
        }),
        __metadata("design:paramtypes", [TodoService])
    ], TodosComponent);
    return TodosComponent;
}());
export { TodosComponent };
//# sourceMappingURL=todos.component.js.map