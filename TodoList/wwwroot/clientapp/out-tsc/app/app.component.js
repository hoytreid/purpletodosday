var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { TodoService } from './todo.service';
var AppComponent = /** @class */ (function () {
    function AppComponent(todoService) {
        this.todoService = todoService;
        this.showCompleted = true;
        this.sortBy = 'newest';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.getTodos();
    };
    AppComponent.prototype.getTodos = function () {
        var _this = this;
        this.todoService.getTodos()
            .subscribe(function (todos) { _this.todos = _this.sortTodos(todos, _this.sortBy); });
    };
    AppComponent.prototype.onAddTodo = function (task) {
        var _this = this;
        this.todoService.addTodo({ task: task })
            .subscribe(function () { return _this.getTodos(); });
    };
    AppComponent.prototype.onSaveTodo = function (todo) {
        this.todoService.editTodo(todo)
            .subscribe();
    };
    AppComponent.prototype.onDeleteTodo = function (todo) {
        var _this = this;
        this.todoService.deleteTodo(todo)
            .subscribe(function () { _this.todos = _this.todos.filter(function (t) { return t !== todo; }); });
    };
    AppComponent.prototype.onToggleCompleted = function (toggle) {
        this.showCompleted = toggle;
    };
    AppComponent.prototype.sortTodos = function (todos, sortBy) {
        switch (sortBy) {
            case "newest": {
                todos.sort(function (a, b) { return (a.id < b.id) ? 1 : ((b.id < a.id) ? -1 : 0); });
                break;
            }
            case "oldest": {
                todos.sort(function (a, b) { return (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0); });
                break;
            }
            case "alphabetical": {
                todos.sort(function (a, b) { return (a.task > b.task) ? 1 : ((b.task > a.task) ? -1 : 0); });
                break;
            }
            case "reverse-alphabetical": {
                todos.sort(function (a, b) { return (a.task < b.task) ? 1 : ((b.task < a.task) ? -1 : 0); });
                break;
            }
        }
        return todos;
    };
    AppComponent.prototype.onSortTodos = function (sort) {
        this.sortBy = sort;
        this.getTodos();
    };
    AppComponent = __decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }),
        __metadata("design:paramtypes", [TodoService])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map