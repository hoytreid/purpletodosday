﻿import { Todo } from './todo';

export const TODOS: Todo[] = [
    { id: 1, task: 'Remake project', complete: false },
    { id: 2, task: 'Code API', complete: true },
    { id: 3, task: 'Add styling', complete: false },
    { id: 4, task: 'Connect projects', complete: false },
    { id: 5, task: 'Test and polish', complete: false }
];