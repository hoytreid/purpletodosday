import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-list-controls',
  templateUrl: './todo-list-controls.component.html',
  styleUrls: ['./todo-list-controls.component.css']
})
export class TodoListControlsComponent {

    @Input()
    showCompleted: boolean;

    @Output()
    toggle: EventEmitter<boolean> = new EventEmitter();

    @Output()
    sort: EventEmitter<string> = new EventEmitter();

    sortParameter: string = 'newest';

    toggleCompleted() {
        this.showCompleted = !this.showCompleted;
        this.toggle.emit(this.showCompleted);
    }

    sortTodos() {
        this.sort.emit(this.sortParameter);
    }
}
