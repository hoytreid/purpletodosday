import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListControlsComponent } from './todo-list-controls.component';

describe('TodoListControlsComponent', () => {
  let component: TodoListControlsComponent;
  let fixture: ComponentFixture<TodoListControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoListControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoListControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
