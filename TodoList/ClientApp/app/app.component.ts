import { Component } from '@angular/core';
import { Todo } from './todo';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    todos: Todo[];
    showCompleted: boolean = true;
    sortBy: string = 'newest';

    constructor(private todoService: TodoService) { }

    public ngOnInit() {
        this.getTodos();
    }

    getTodos() {
        this.todoService.getTodos()
            .subscribe(todos => { this.todos = this.sortTodos(todos, this.sortBy) });
    }

    onAddTodo(task: string) {
        this.todoService.addTodo({ task } as Todo)
            .subscribe(() => this.getTodos());
    }

    onSaveTodo(todo: Todo) {
        this.todoService.editTodo(todo)
            .subscribe();
    }

    onDeleteTodo(todo: Todo) {
        this.todoService.deleteTodo(todo)
            .subscribe(() => { this.todos = this.todos.filter(t => t !== todo) });
    }

    onToggleCompleted(toggle: boolean) {
        this.showCompleted = toggle;
    }

    sortTodos(todos: Todo[], sortBy: string) {
        switch (sortBy) {
            case "newest": {
                todos.sort((a, b) => (a.id < b.id) ? 1 : ((b.id < a.id) ? -1 : 0));
                break;
            }
            case "oldest": {
                todos.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
                break;
            }
            case "alphabetical": {
                todos.sort((a, b) => (a.task > b.task) ? 1 : ((b.task > a.task) ? -1 : 0));
                break;
            }
            case "reverse-alphabetical": {
                todos.sort((a, b) => (a.task < b.task) ? 1 : ((b.task < a.task) ? -1 : 0));
                break;
            }
        }
        return todos;
    }

    onSortTodos(sort: string) {
        this.sortBy = sort;
        this.getTodos();
    }
}
