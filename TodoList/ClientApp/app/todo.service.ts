import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Todo } from './todo';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TodoService {

    constructor(private http: HttpClient) { }
    private todosUrl = 'http://localhost:50326/api/todos';

    getTodos(): Observable<Todo[]> {
        return this.http.get<Todo[]>(this.todosUrl);
    }

    editTodo(todo: Todo): Observable<any> {
        return this.http.put(`${this.todosUrl}/${todo.id}`, todo, httpOptions)
    }

    addTodo(todo: Todo): Observable<Todo> {
        return this.http.post<Todo>(this.todosUrl, todo, httpOptions);
    }

    deleteTodo(todo: Todo ): Observable<Todo> {
        return this.http.delete<Todo>(`${this.todosUrl}/${todo.id}`, httpOptions);
    }
}
