import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {

    @Input()
    todos: Todo[];

    @Input()
    showCompleted: boolean;

    @Output()
    delete: EventEmitter<Todo> = new EventEmitter();

    @Output()
    save: EventEmitter<Todo> = new EventEmitter();

    onSaveTodo(todo: Todo) {
        this.save.emit(todo);
    }

    onDeleteTodo(todo: Todo) {
        this.delete.emit(todo);
    }
}
