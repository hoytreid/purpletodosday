import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../../todo';

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.css']
})
export class TodoListItemComponent {

    @Input()
    todo: Todo;

    @Output()
    delete: EventEmitter<Todo> = new EventEmitter();

    @Output()
    save: EventEmitter<Todo> = new EventEmitter();

    editing: boolean = false;
    backupTodo: Todo;

    toggleComplete() {
        this.save.emit(this.todo);
    }

    editTodo() {
        this.editing = true;
        this.backupTodo = JSON.parse(JSON.stringify(this.todo))
    }

    cancelEdit() {
        this.editing = false;
        this.todo = this.backupTodo;
    }

    saveTodo() {
        this.editing = false;
        this.save.emit(this.todo);
    }

    deleteTodo() {
        this.delete.emit(this.todo);
    }

}