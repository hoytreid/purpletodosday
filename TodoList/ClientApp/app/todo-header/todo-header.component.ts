import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-header',
  templateUrl: './todo-header.component.html',
  styleUrls: ['./todo-header.component.css']
})
export class TodoHeaderComponent {

    @Output()
    add: EventEmitter<string> = new EventEmitter();

    headline = 'What needs doing?';

    addTodo(task: string) {
        this.add.emit(task);
    }
}
