﻿export class Todo {
    id: number;
    task: string;
    complete: boolean;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}